import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from 'pages/Home'


Vue.use(VueRouter)

  const routes = [
	{
    path: '/',
		component: Home,
		name: 'Home'
  	},
	{
    path: '/category/:name/',
		props: true,
		name: 'Category',
		component: () => import(/* webpackChunkName: "category" */ '../views/Category.vue')
	},
	{
		path: '/single/',
		props: true,
		name: 'Single'
	}
]

const router = new VueRouter({
  	mode: 'history',
  	base: process.env.BASE_URL,
  	routes
})

export default router
